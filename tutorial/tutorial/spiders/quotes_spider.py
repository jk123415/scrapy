import scrapy
from scrapy.linkextractors import LinkExtractor


class QuotesSpider(scrapy.Spider):
    name = "quotes"
    db_name = "test.db"
    table_name = "t1"

    def start_requests(self):
        urls = [
            'https://www.78zw.com/5_5988/',
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        pattern = r'/d+\.html'
        link = LinkExtractor(restrict_css="#list")
        links = link.extract_links(response)
        for link in links:
            yield scrapy.Request(link.url, callback=self.i_parse, meta={
                'link': link
            })

    def i_parse(self, response):
        item = {}
        link = response.meta.get("link")
        item['id'] = link.url
        item['title'] = link.text
        item['content'] = response.css("#content::text").getall()
        return item
